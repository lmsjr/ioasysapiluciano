﻿CREATE TABLE [dbo].[Vote]
(
	[VoteId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [UserId] INT NOT NULL, 
    [MovieId] INT NOT NULL, 
    [Note] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
)
