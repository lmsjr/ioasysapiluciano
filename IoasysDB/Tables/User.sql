﻿CREATE TABLE [dbo].[User]
(
	[UserId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    [Name] VARCHAR(100) NOT NULL, 
    [Email] VARCHAR(100) NOT NULL, 
    [Password] VARCHAR(100) NOT NULL, 
    [Administrator] BIT NOT NULL, 
    [Enabled] BIT NOT NULL, 
    [CreatedBy] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [ModifiedBy] INT NULL, 
    [ModifiedOn] DATETIME NULL
)
