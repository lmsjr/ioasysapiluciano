﻿CREATE TABLE [dbo].[MovieActors]
(
	[MovieActorsId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY,
    [MovieId] INT NOT NULL, 
    [ActorId] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL
)
