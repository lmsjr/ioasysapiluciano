﻿CREATE TABLE [dbo].[Movie]
(
	[MovieId] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [GenreId] INT NOT NULL, 
    [Name] VARCHAR(100) NOT NULL, 
    [Director] VARCHAR(100) NOT NULL, 
    [Enabled] BIT NOT NULL, 
    [CreatedBy] INT NOT NULL, 
    [CreatedOn] DATETIME NOT NULL, 
    [ModifiedBy] INT NULL, 
    [ModifiedOn] DATETIME NULL
)
