﻿using IoasysAPP.IRepository;
using IoasysAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.Repository
{
    public class GenreRepository : IGenreRepository
    {
        private readonly Context _Context;
        public GenreRepository(Context Context)
        {
            _Context = Context;
        }

        public IEnumerable<Genre> List()
        {
            try
            {
                return _Context._Genre.ToList().OrderBy(x => x.Name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Genre Get(int Id)
        {
            try
            {
                return _Context._Genre.FirstOrDefault(p => p.GenreId == Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Add(Genre _Genre)
        {
            _Context._Genre.Add(_Genre);
            _Context.SaveChanges();
            return _Genre.GenreId;
        }
    }
}
