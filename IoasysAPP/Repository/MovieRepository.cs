﻿using IoasysAPP.Helper;
using IoasysAPP.IRepository;
using IoasysAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.Repository
{
    public class MovieRepository : IMovieRepository
    {
        private readonly Context _Context;

        public MovieRepository(Context Context)
        {
            _Context = Context;
        }

        public IEnumerable<Movie> List(Filter _Filter)
        {
            try
            {
                _Filter.Size = (_Filter.Size == 0) ? 5 : _Filter.Size;

                var Movies = _Context._Movie
                    .Skip(_Filter.Start)
                    .Take(_Filter.Size)
                    .ToList();

                foreach(Movie M in Movies)
                {
                    List<MovieActors> MAL = new List<MovieActors>();

                    var MovieActors = _Context._MovieActors.Where(x => x.MovieId == M.MovieId);

                    foreach (MovieActors MA in MovieActors)
                    {
                        MAL.Add(MA);
                    }

                    M.MovieActors = MAL;

                    M.VotesNumber = _Context._Vote.Where(x => x.MovieId == M.MovieId).Sum(x => x.Note);
                }

                return Movies.OrderBy(x => x.VotesNumber).OrderBy(x => x.Name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Movie Get(int Id)
        {
            try
            {
                return _Context._Movie.FirstOrDefault(p => p.MovieId == Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Add(Movie _Movie)
        {
            _Movie.CreatedOn = DateTime.Now;
            _Context._Movie.Add(_Movie);
            _Context.SaveChanges();

            if (_Movie.MovieActors != null && _Movie.MovieActors.Count > 0)
            {
                foreach (var Actors in _Movie.MovieActors)
                {
                    Actors.CreatedOn = DateTime.Now;
                    Actors.MovieId = _Movie.MovieId;
                    _Context._MovieActors.Add(Actors);
                    _Context.SaveChanges();
                }
            }

            return _Movie.MovieId;
        }
    }
}
