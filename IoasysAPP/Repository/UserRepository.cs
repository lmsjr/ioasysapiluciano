﻿using IoasysAPP.Helper;
using IoasysAPP.IRepository;
using IoasysAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly Context _Context;
        public UserRepository(Context Context)
        {
            _Context = Context;
        }

        public IEnumerable<User> List(Filter _Filter)
        {
            try
            {
                _Filter.Size = (_Filter.Size == 0) ? 5 : _Filter.Size;

                return _Context._User.Where(x => x.Enabled == true && x.Administrator == false)
                                     .OrderBy(x => x.Name)
                                     .Skip(_Filter.Start)
                                     .Take(_Filter.Size)
                                     .ToList();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public User Get(int Id)
        {
            try
            {
                return _Context._User.FirstOrDefault(p => p.UserId == Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Add(User _User)
        {
            _User.CreatedOn = DateTime.Now;
            _User.Password = Settings.Encrypt(_User.Password);

            _Context._User.Add(_User);
            _Context.SaveChanges();
            return _User.UserId;
        }        

        public void Update(User _User)
        {
            _User.ModifiedOn = DateTime.Now;

            _Context._User.Update(_User);
            _Context.SaveChanges();
        }

        public void Remove(int Id)
        {
            var _User = _Context._User.First(p => p.UserId == Id);
            _Context._User.Remove(_User);
            _Context.SaveChanges();
        }
    }
}
