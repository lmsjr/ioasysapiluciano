﻿using IoasysAPP.IRepository;
using IoasysAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.Repository
{
    public class VoteRepository : IVoteRepository
    {
        private readonly Context _Context;

        public VoteRepository(Context Context)
        {
            _Context = Context;
        }

        public int Add(Vote _Vote)
        {
            _Vote.CreatedOn = DateTime.Now;
            _Context._Vote.Add(_Vote);
            _Context.SaveChanges();

            return _Vote.VoteId;
        }
    }
}
