﻿using IoasysAPP.Helper;
using IoasysAPP.IRepository;
using IoasysAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.Repository
{
    public class LoginRepository : ILoginRepository
    {
        private readonly Context _Context;
        public LoginRepository(Context Context)
        {
            _Context = Context;
        }

        public User Login(string Email, string Password)
        {
            try
            {
                string Pass = Settings.Encrypt(Password);
                return _Context._User.FirstOrDefault(p => p.Email == Email && p.Password == Pass);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
