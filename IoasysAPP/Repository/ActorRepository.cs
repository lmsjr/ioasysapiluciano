﻿using IoasysAPP.IRepository;
using IoasysAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.Repository
{
    public class ActorRepository : IActorRepository
    {
        private readonly Context _Context;
        public ActorRepository(Context Context)
        {
            _Context = Context;
        }

        public IEnumerable<Actor> List()
        {
            try
            {
                return _Context._Actor.ToList().OrderBy(x => x.Name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Actor Get(int Id)
        {
            try
            {
                return _Context._Actor.FirstOrDefault(p => p.ActorId == Id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Add(Actor _Actor)
        {
            _Context._Actor.Add(_Actor);
            _Context.SaveChanges();
            return _Actor.ActorId;
        }
    }
}
