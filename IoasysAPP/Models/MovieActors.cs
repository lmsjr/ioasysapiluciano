﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.Models
{
    public class MovieActors
    {
        public int MovieActorsId {get; set;}
        public int MovieId { get; set; }
        public int ActorId { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
