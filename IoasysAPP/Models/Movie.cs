﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.Models
{
    public class Movie
    {
        public int MovieId { get; set; }
        public int GenreId { get; set; }
        public string Name { get; set; }
        public string Director { get; set; }
        public bool Enabled { get; set; }       
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        [NotMapped]
        public int VotesNumber { get; set; }
        [NotMapped]
        public List<MovieActors> MovieActors { get; set; }
    }
}
