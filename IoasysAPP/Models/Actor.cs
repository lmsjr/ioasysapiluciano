﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.Models
{
    public class Actor
    {
        public int ActorId { get; set; }
        public string Name { get; set; }
    }
}
