﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IoasysAPP.Models;
using Microsoft.EntityFrameworkCore;

namespace IoasysAPP.Models
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> Options) : base(Options)
        {

        }

        public DbSet<User> _User { get; set; }
        public DbSet<Movie> _Movie { get; set; }
        public DbSet<Actor> _Actor { get; set; }
        public DbSet<Genre> _Genre { get; set; }
        public DbSet<Vote> _Vote { get; set; }
        public DbSet<MovieActors> _MovieActors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Movie>().ToTable("Movie");
            modelBuilder.Entity<Genre>().ToTable("Genre");
            modelBuilder.Entity<Vote>().ToTable("Vote");
            modelBuilder.Entity<Actor>().ToTable("Actor");
            modelBuilder.Entity<MovieActors>().ToTable("MovieActors");

            //modelBuilder.Entity<MovieActors>().HasKey(x => new { x.MovieId, x.ActorId });

            //modelBuilder.Entity<MovieActors>().HasOne(bc => bc.Movie)
            //                                  .WithMany(b => b.MovieActors)
            //                                  .HasForeignKey(bc => bc.MovieId);
            //modelBuilder.Entity<MovieActors>().HasOne(bc => bc.Actor)
            //                                 .WithMany(c => c.MovieActors)
            //                                 .HasForeignKey(bc => bc.ActorId);

            //modelBuilder.Entity<Movie>().HasMany(x => x.Votes).WithOne(x => x.Movie);
        }
    }
}
