﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.Models
{
    public class Vote
    {
        public int VoteId { get; set; }
        public int UserId { get; set; }
        public int MovieId { get; set; }
        public int Note { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
