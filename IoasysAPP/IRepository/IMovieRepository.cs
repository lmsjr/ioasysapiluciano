﻿using IoasysAPP.Helper;
using IoasysAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.IRepository
{
    public interface IMovieRepository
    {
        IEnumerable<Movie> List(Filter _Filter);
        Movie Get(int Id);
        int Add(Movie _Movie);
    }
}
