﻿using IoasysAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.IRepository
{
    public interface ILoginRepository
    {
        User Login(string Email, string Password);
    }
}
