﻿using IoasysAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.IRepository
{
    public interface IVoteRepository
    {
        int Add(Vote _Vote);
    }
}
