﻿using IoasysAPP.Helper;
using IoasysAPP.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace IoasysAPP.IRepository
{
    public interface IUserRepository
    {
        IEnumerable<User> List(Filter _Filter);
        User Get(int Id);
        int Add(User _User);
        void Update(User _User);
        void Remove(int Id);
    }
}
