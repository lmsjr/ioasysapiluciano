﻿using IoasysAPP.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace IoasysAPP.IRepository
{
    public interface IActorRepository
    {
        IEnumerable<Actor> List();
        Actor Get(int Id);
        int Add(Actor _Actor);
    }
}
