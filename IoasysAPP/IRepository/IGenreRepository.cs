﻿using IoasysAPP.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace IoasysAPP.IRepository
{
    public interface IGenreRepository
    {
        IEnumerable<Genre> List();
        Genre Get(int Id);
        int Add(Genre _Actor);
    }
}
