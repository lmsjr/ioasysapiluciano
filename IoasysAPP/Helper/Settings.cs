﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IoasysAPP.Helper
{
    public static class Settings
    {
        public static string Secret = "fedaf7d8863b48e197b9287d492b708e";

        public static string Encrypt(string text)
        {
            var md5 = new MD5CryptoServiceProvider();
 
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
 
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            { 
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }
    }    
}
