﻿using IoasysAPP.IRepository;
using IoasysAPP.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IoasysAPP.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VoteController : Controller
    {
        private readonly IVoteRepository _VoteRepository;

        public VoteController(IVoteRepository VoteRepository)
        {
            _VoteRepository = VoteRepository;
        }

        [HttpPost]
        [Authorize(Roles = "False")]
        public IActionResult Create(int MovieId, int Note)
        {
            try
            {
                if (MovieId > 0 && Note >= 0)
                {
                    Vote _Vote = new Vote
                    {
                        UserId = GetUserId(),
                        MovieId = MovieId,
                        Note = Note
                    };

                    _VoteRepository.Add(_Vote);

                    return CreatedAtAction("Create", new
                    {
                        success = true,
                        data = _Vote
                    });
                }
                else
                {
                    return BadRequest(new
                    {
                        success = false,
                        errors = "Empty MovieId or Empty Note"
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        private int GetUserId()
        {
            int UserId = 0;

            if (HttpContext.User.Identity is ClaimsIdentity identity)
            {
                if (identity.FindFirst("UserId") != null)
                {
                    Int32.TryParse(identity.FindFirst("UserId").Value, out UserId);
                }
            }

            return UserId;
        }
    }
}
