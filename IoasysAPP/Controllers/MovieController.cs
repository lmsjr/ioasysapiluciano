﻿using IoasysAPP.Helper;
using IoasysAPP.IRepository;
using IoasysAPP.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IoasysAPP.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MovieController : Controller
    {
        private readonly IMovieRepository _MovieRepository;
        private readonly IActorRepository _ActorRepository;
        private readonly IGenreRepository _GenreRepository;

        public MovieController(IMovieRepository MovieRepository, IGenreRepository GenreRepository, IActorRepository ActorRepository)
        {
            _MovieRepository = MovieRepository;
            _ActorRepository = ActorRepository;
            _GenreRepository = GenreRepository;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult<IEnumerable<Movie>> List([FromQuery] Filter _Filter)
        {
            try
            {
                return _MovieRepository.List(_Filter).ToList();
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        [HttpGet("{id}")]
        [Authorize]        
        public ActionResult<Movie> Get(int id)
        {
            try
            {
                Movie Usr = _MovieRepository.Get(id);

                if (Usr == null)
                {
                    return NotFound();
                }
                else
                {
                    return Usr;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        [HttpPost]
        [Authorize(Roles = "True")]
        public IActionResult Create([FromBody] Movie _Movie)
        {
            try
            {
                if (_Movie != null)
                {
                    var Genre = _GenreRepository.Get(_Movie.GenreId);

                    if (Genre == null)
                    {
                        return NotFound("Genre not found, GenreId: " + _Movie.GenreId);
                    }

                    foreach(var Actor in _Movie.MovieActors)
                    {
                        var Act = _ActorRepository.Get(Actor.ActorId);

                        if(Act == null)
                        {
                            return NotFound("Actor not found, ActorId: " + Actor.ActorId);
                        }
                    }

                    _Movie.CreatedBy = GetUserId();
                    _MovieRepository.Add(_Movie);

                    return CreatedAtAction("Create", new
                    {
                        success = true,
                        data = _Movie
                    });
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        private int GetUserId()
        {
            int UserId = 0;

            if (HttpContext.User.Identity is ClaimsIdentity identity)
            {
                if (identity.FindFirst("UserId") != null)
                {
                    Int32.TryParse(identity.FindFirst("UserId").Value, out UserId);
                }
            }

            return UserId;
        }
    }
}
