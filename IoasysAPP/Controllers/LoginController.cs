﻿using IoasysAPP.Helper;
using IoasysAPP.IRepository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoasysAPP.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILoginRepository _LoginRepository;

        public LoginController(ILoginRepository UserRepository)
        {
            _LoginRepository = UserRepository;
        }

        [HttpPost]
        [Route("login")]
        public ActionResult<dynamic> Authenticate(string Email, string Password)
        {
            var user = _LoginRepository.Login(Email, Password);

            if (user == null)
            {
                return NotFound
                (
                    new 
                    { 
                        message = "Usuário ou senha inválidos" 
                    }
                );
            }

            var token = TokenService.GenerateToken(user);

            user.Password = "";

            return new
            {
                user = user,
                token = token
            };
        }
    }
}
