﻿using IoasysAPP.Helper;
using IoasysAPP.IRepository;
using IoasysAPP.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IoasysAPP.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private readonly IUserRepository _UserRepository;

        public UserController(IUserRepository UserRepository)
        {
            _UserRepository = UserRepository;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult<IEnumerable<User>> List([FromQuery] Filter _Filter)
        {
            try
            {
                return _UserRepository.List(_Filter).ToList();
            }
            catch(Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<User> Get(int id)
        {
            try
            {
                User Usr = _UserRepository.Get(id);

                if(Usr == null)
                {
                    return NotFound();
                }
                else
                {
                    return Usr;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        [HttpPost]
        [Authorize]
        public IActionResult Create([FromBody] User _User)
        {
            try
            {
                if (_User != null)
                {
                    _User.CreatedBy = GetUserId();
                    _UserRepository.Add(_User);

                    return CreatedAtAction("Create", new
                    {
                        success = true,
                        data = _User
                    });
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        [HttpPut]
        [Authorize]
        public IActionResult Update([FromBody] User _User)
        {
            try
            {
                if (_User != null)
                {
                    _User.CreatedBy = GetUserId();
                    User Usr = _UserRepository.Get(_User.UserId);

                    if (Usr != null)
                    {                        
                        _UserRepository.Update(Usr);
                        return CreatedAtAction("Update", new
                        {
                            success = true,
                            data = _User
                        });
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return BadRequest(new
                    {
                        success = false,
                        errors = "Empty data"
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        [HttpDelete]
        [Authorize]
        public IActionResult Delete(int id)
        {
            try
            {
                if (id > 0)
                {
                    User Usr = _UserRepository.Get(id);

                    if (Usr != null)
                    {
                        Usr.Enabled = false;
                        Usr.ModifiedBy = GetUserId();
                        Usr.ModifiedOn = DateTime.Now;

                        _UserRepository.Update(Usr);

                        return CreatedAtAction("Delete", new
                        {
                            success = true,
                            data = Usr
                        });
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return BadRequest(new
                    {
                        success = false,
                        errors = "Empty id"
                    });
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        private int GetUserId()
        {
            int UserId = 0;

            if (HttpContext.User.Identity is ClaimsIdentity identity)
            {
                if (identity.FindFirst("UserId") != null)
                {
                    Int32.TryParse(identity.FindFirst("UserId").Value, out UserId);
                }
            }

            return UserId;
        }
    }
}
