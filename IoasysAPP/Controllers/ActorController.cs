﻿using IoasysAPP.IRepository;
using IoasysAPP.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IoasysAPP.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ActorController : Controller
    {
        private readonly IActorRepository _ActorRepository;

        public ActorController(IActorRepository ActorRepository)
        {
            _ActorRepository = ActorRepository;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult<IEnumerable<Actor>> List()
        {
            try
            {
                return _ActorRepository.List().ToList();
            }
            catch(Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<Actor> Get(int id)
        {
            try
            {
                Actor Act = _ActorRepository.Get(id);

                if(Act == null)
                {
                    return NotFound();
                }
                else
                {
                    return Act;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        [HttpPost]
        [Authorize]
        public IActionResult Create([FromBody] Actor _Actor)
        {
            try
            {
                if (_Actor != null)
                {
                    _ActorRepository.Add(_Actor);

                    return CreatedAtAction("Create", new
                    {
                        success = true,
                        data = _Actor
                    });
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }        
    }
}
