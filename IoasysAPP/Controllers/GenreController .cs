﻿using IoasysAPP.IRepository;
using IoasysAPP.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IoasysAPP.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GenreController : Controller
    {
        private readonly IGenreRepository _GenreRepository;

        public GenreController(IGenreRepository GenreRepository)
        {
            _GenreRepository = GenreRepository;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult<IEnumerable<Genre>> List()
        {
            try
            {
                return _GenreRepository.List().ToList();
            }
            catch(Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        [HttpGet("{id}")]
        [Authorize]
        public ActionResult<Genre> Get(int id)
        {
            try
            {
                Genre Act = _GenreRepository.Get(id);

                if(Act == null)
                {
                    return NotFound();
                }
                else
                {
                    return Act;
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }

        [HttpPost]
        [Authorize]
        public IActionResult Create([FromBody] Genre _Genre)
        {
            try
            {
                if (_Genre != null)
                {
                    _GenreRepository.Add(_Genre);

                    return CreatedAtAction("Create", new
                    {
                        success = true,
                        data = _Genre
                    });
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    success = false,
                    errors = ex.Message
                });
            }
        }        
    }
}
